package com.example.employeedata.dataaccess;

import com.example.employeedata.controllers.EmployeeInterface;
import com.example.employeedata.models.domain.Employee;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class EmployeeDataRepository implements EmployeeInterface {

    public static Map<Integer, Employee> employeeMap;
    {
        employeeMap = new HashMap<>();
        employeeMap.put(1, new Employee(1, "R2", "D2"));
        employeeMap.put(2, new Employee(2, "Ludvig", "Bowallius"));
        employeeMap.put(3, new Employee(3, "Thomas", "Andersson"));
    }

    @Override
    public HashMap<Integer, Employee> getAllEmployees() {
        return (HashMap<Integer, Employee>) employeeMap;
    }

    @Override
    public Employee getEmployee(int id) {
        return employeeMap.get(id);
    }

    @Override
    public Employee createEmployee(int id, Employee employee) {
    employeeMap.put(id, employee);
    return getEmployee(employee.getId());
    }

    @Override
    public Employee replaceEmployee(int id, Employee employee) {
        //Unsure if this is needed, need to try it out
        //var employeeToDelete = getEmployee(employee.getId());
        employeeMap.replace(id, employee);
        return getEmployee(employee.getId());

    }

    @Override
    public Employee modifyEmployee(Employee employee) {
        var employeeToModify = getEmployee(employee.getId());
        //WHAT IS REASONABLE TO ADD HERE BELOW?

        //guitarToModify.setModel(guitar.getModel());
        //guitarToModify.setBrand(guitar.getBrand());

        return employee;

    }





    public static boolean employeeExistsById(int id) {
        return employeeMap.containsKey(id);
    }
    //Remember to implement luhn validation here

/*    public static boolean employeeExists(Employee employee) {
        return employee.getId() > 0 && employee.getUniqueId() > 0 && employee.getFirstName() != null && employee.getLastName() != null && employee.getDob() != null && employee.getEmployeeTimerType() != null && employee.getPosition() != null && employee.getDateHired() != null && employee.getDevicesUsed() != null;
    }*/

    public static boolean isActualEmployee(Employee employee) {
        return employee.getId() > 0 && employee.getUniqueCode() > 0 && employee.getFirstName() != null && employee.getLastName() != null && employee.getDob() != null && employee.getEmployeeTimerType() != null && employee.getPosition() != null && employee.getDateHired() != null && employee.getDevicesUsed() != null;
    }

    public static boolean employeeExists(Employee employee) {
        return employeeMap.containsKey(employee.getId());
    }

    //public boolean isValidGuitar(Guitars guitar, int id) {
    //    return isValidGuitar(guitar) && guitar.getId() == id;}

}
