package com.example.employeedata.controllers;

import com.example.employeedata.dataaccess.EmployeeDataRepository;
import com.example.employeedata.models.domain.Employee;
import com.example.employeedata.models.domain.FakeEmployee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping(value = "/employees")
public class EmployeeController {

        private EmployeeDataRepository repository;

        @Autowired
        public EmployeeController(EmployeeDataRepository repository) {this.repository = repository;}

        @GetMapping("")
        public ResponseEntity<HashMap> getAllEmployees() {
            return new ResponseEntity<>(repository.getAllEmployees(), HttpStatus.OK);
        }

        @GetMapping("/{value}")
        public ResponseEntity<Employee> getEmployee(@PathVariable int value) {
        if (!repository.employeeExistsById(value)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(repository.getEmployee(value), HttpStatus.OK);

    }

        @PostMapping("/create")
        public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee) {
        if (!repository.isActualEmployee(employee)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        int value = employee.getId();

        return new ResponseEntity<>(repository.createEmployee(value, employee), HttpStatus.OK);
    }

    @PostMapping("/fakecreate")
    public FakeEmployee fakeCreateEmployee(@RequestBody FakeEmployee employee) {

            return employee;

    }

    @PutMapping("/{id}")
    public ResponseEntity<Employee> replaceEmployee(@PathVariable int id, @RequestBody Employee employee) {
        if (!repository.isActualEmployee(employee)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(repository.replaceEmployee(id, employee),HttpStatus.OK);
    }

    @PatchMapping ("/{value}")
    public ResponseEntity<Employee> modifyEmployee(@RequestBody Employee employee) {

        if (!repository.isActualEmployee(employee)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(repository.modifyEmployee(employee),HttpStatus.OK);
    }






}