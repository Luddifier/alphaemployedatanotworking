package com.example.employeedata.controllers;

import com.example.employeedata.dataaccess.EmployeeDataRepository;
import com.example.employeedata.models.domain.Employee;

import java.util.HashMap;

public interface EmployeeInterface {

    HashMap<Integer, Employee> getAllEmployees();
    Employee createEmployee(int id, Employee employee);
    Employee replaceEmployee(int id, Employee employee);
    Employee getEmployee(int id);

    Employee modifyEmployee(Employee employee);
}
