package com.example.employeedata.models.domain;

import java.util.List;

public class FakeEmployee {
    private int id;

    public FakeEmployee(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
